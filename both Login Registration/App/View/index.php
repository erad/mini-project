<?php 
session_start();
function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use App\Engine\Core\BluePrint;
use App\Engine\Tool\Debug;
use App\Engine\Tool\Q;

if($_SERVER['REQUEST_METHOD']==='GET' && isset($_SESSION['user']) && !empty($_SESSION['user']) )
{
	//echo "OK"."<br/>";
	//Debug::d($_SESSION);
	$_user = $_SESSION['user'];
	//echo $_SESSION['user']."<br/>";

	$array=[
		'tbl'=>'tbl_user',
		//'fields'=>['email','password'],
		'where'=>[ 'username'=>$_user ],
		//'like'=>['username'=>'mi'],
		//'order'=>['password'=>'DESC' , 'email'=>'DESC'],
	];
	
	$obj = new BluePrint;
	$result = $obj->index($array);
	$user=$result[0];
	//Debug::pd($result);

	
}else{
	header("location: login.php");
}


?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Profile</title>

		<!-- Bootstrap -->
		<link href="./../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="./../Resource/font_awesome/css/font_awesome.min.css" rel="stylesheet" type="text/css"/>
	</head>
	
	<body>
		
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">BITM</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="edit.php"><i class="fa fa-pencil"></i>&nbsp; Edit Profile </a></li>
            <li><a href="print.php"><i class="fa fa-print"></i>&nbsp; Print CV</a></li>
            <li><a href="#"><i class="fa fa-wrench"></i>&nbsp; Settings</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="logout.php"><i class="fa fa-power-off"></i>&nbsp; Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		
		<div class="container">
			<div class="col-md-6 col-md-offset-3">
			
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-user fa-lg"></i>&nbsp; <?php echo $user->fullname;?>
						</h3>
					</div>
					<div class="panel-body">
						<table align="center">
							<tr>
								<td>Username</td>
								<td>&nbsp; : &nbsp;</td>
								<td><?php echo $user->username;?></td>
							</tr>
							<tr>
								<td>Email</td>
								<td>&nbsp; : &nbsp;</td>
								<td><?php echo $user->email;?></td>
							</tr>
							<tr>
								<td>Birth Date</td>
								<td>&nbsp; : &nbsp;</td>
								<?php 
									$db_date = strtotime($user->birthdate);
									$Date = date( 'd M Y', $db_date );
								?>
								<td><?php echo $Date?></td>
							</tr>
						</table>
					</div>
					<div class="panel-footer text-center text-info">
						<i class="fa fa-code fa-lg">The Code Squad</i>
					</div>
				</div>
			
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="./../Resource/jquery/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="./../Resource/bootstrap/js/bootstrap.min.js"></script>
		
		
		<script type="text/JavaScript">
			
		</script>
		
	</body>
	
</html>
