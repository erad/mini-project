<?php 

function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use App\Engine\Core\BluePrint;
use App\Engine\Tool\Debug;
use App\Engine\Tool\Q;

//Debug::d(123);

?>
<?php 
session_start();
if(isset($_SESSION['user'])){
	die("<h1 style='color:red'>You already own an account!</h1>");
}

$fullname_value = isset($_SESSION['fullname_value'])?$_SESSION['fullname_value']:"";
$fullname_class = isset($_SESSION['fullname_class'])?$_SESSION['fullname_class']:"success";
$fullname_msg = isset($_SESSION['fullname_msg'])?$_SESSION['fullname_msg']:"Please Enter Your Full Name";

$birthdate_value = isset($_SESSION['birthdate_value'])?$_SESSION['birthdate_value']:"";
$birthdate_class = isset($_SESSION['birthdate_class'])?$_SESSION['birthdate_class']:"success";
$birthdate_msg = isset($_SESSION['birthdate_msg'])?$_SESSION['birthdate_msg']:"Please enter your birth date";

$email_value = isset($_SESSION['email_value'])?$_SESSION['email_value']:"";
$email_class = isset($_SESSION['email_class'])?$_SESSION['email_class']:"success";
$email_msg = isset($_SESSION['email_msg'])?$_SESSION['email_msg']:"Please enter your Email";

$username_value = isset($_SESSION['username_value'])?$_SESSION['username_value']:"";
$username_class = isset($_SESSION['username_class'])?$_SESSION['username_class']:"success";
$username_msg = isset($_SESSION['username_msg'])?$_SESSION['username_msg']:"Please enter a username";

$password_class = isset($_SESSION['password_class'])?$_SESSION['password_class']:"success";
$password_msg = isset($_SESSION['password_msg'])?$_SESSION['password_msg']:"Please enter a password";

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Registration</title>

		<!-- Bootstrap -->
		<link href="./../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="./../Resource/font_awesome/css/font_awesome.min.css" rel="stylesheet" type="text/css"/>
	</head>
	
	<body>
		
		
		<div class="container">
			<div class="col-md-6 col-md-offset-3">
			<br/>
			<br/>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<span class="glyphicon glyphicon-user "></span> User Registration
						</h3>
					</div>
					<div class="panel-body">

						<form action="reg.php" method="POST" >
							<div class="form-group has-<?php echo $fullname_class?>">
								<label class="control-label" for="fullname">Full Name</label>
								<input type="text" name="fullname" value="<?php echo $fullname_value;?>" placeholder="Title Full Name Surname" class="form-control" id="fullname" aria-describedby="fullname" autofocus />
								<span id="fullname" class="help-block"><?php echo $fullname_msg;?></span>
							</div>
							<div class="form-group has-<?php echo $birthdate_class?>">
								<label class="control-label" for="birthdate">Birth Date</label>
								<input type="date" name="birthdate" value="<?php echo $birthdate_value;?>" class="form-control" id="birthdate" aria-describedby="birthdate" />
								<span id="birthdate" class="help-block"><?php echo $birthdate_msg;?></span>
							</div>
							<div class="form-group has-<?php echo $email_class?>">
								<label class="control-label" for="email">Email</label>
								<input type="email" name="email" value="<?php echo $email_value;?>" placeholder="example@email.com" class="form-control" id="email" aria-describedby="email" />
								<span id="email" class="help-block"><?php echo $email_msg;?></span>
							</div>
							<div class="form-group has-<?php echo $username_class?>">
								<label class="control-label" for="username">Username</label>
								<input type="text" name="username" value="<?php echo $username_value;?>" placeholder="username" class="form-control" id="username" aria-describedby="username" />
								<span id="username" class="help-block"><?php echo $username_msg;?></span>
							</div>
							<div class="form-group has-<?php echo $password_class?>">
								<label class="control-label" for="password">Password</label>
								<input type="password" name="password" value="" placeholder="password" class="form-control" id="password" aria-describedby="password" />
								<span id="password" class="help-block"><?php echo $password_msg;?></span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="agree" value="agree" required/> Agree <a href="#">Terms & Conditions</a>.
								</label>
							</div>
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-ok"></span> Submit
							</button>
							<button type="reset" class="btn btn-default">
								<span class="glyphicon glyphicon-refresh"></span> Reset
							</button>
						</form>		
						
						<p class="text-center">
							Already have an account? Please <a href="login.php">Login Here</a>
						</p>			
					
					</div>
					<div class="panel-footer text-center">
						<i class="fa fa-code fa-lg">The Code Squad</i>
					</div>
				</div>
			
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="./../Resource/jquery/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="./../Resource/bootstrap/js/bootstrap.min.js"></script>
		
		
		<script type="text/JavaScript">
			
		</script>
		
	</body>
	
</html>