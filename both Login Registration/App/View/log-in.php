<?php 
session_start();

function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use App\Engine\Core\BluePrint;
use App\Engine\Tool\Debug;
use App\Engine\Tool\Q;

if($_SERVER['REQUEST_METHOD']==='POST'){
	$login = $_POST;
	if(count($login)===2 && array_key_exists('username',$login) && array_key_exists('password',$login)  
			&& !empty($login['username']) && !empty($login['password'] )
	){
		//Debug::pd($login);
		$q_arr = [
			'tbl'=>'tbl_user',
			//'fields'=>['username','password'],
			'where'=>['username'=>$login['username'] , 'password'=>$login['password'] ],
		];
		$login_result = Q::select($q_arr);
		if(count($login_result)===1){
			$_SESSION['login_attempt']=true;
			$_SESSION['user']=$login['username'];
			header("location: index.php");
		}else{
			$_SESSION['login_attempt']=false;
			header("location: login.php");
		}
	}else{
		$_SESSION['login_attempt']=false;
		header("location: login.php");
	}
}



?>