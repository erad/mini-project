<?php 

namespace App\Engine\Tool;

class Q
{
	static public function test(){
		echo "Query Ready";
	}
	
	static public function dbS(){
		mysql_connect("127.0.0.1" , "root" , "") or die("Database Server Error!");
		mysql_select_db("dangerous") or die("Database Not Found!!!");
		//echo "Database Connected";
	}
	
	static public function select($act){
		if(is_array($act) && count($act)>0 && array_key_exists('tbl',$act) && !empty($act['tbl']) )
		{
			
			$tbl = $act['tbl'];
			$fields = [];
			if(array_key_exists('fields',$act) && is_array($act['fields']) && count($act['fields'])>0 )
			{
				foreach($act['fields'] AS $field){
					$fields[]="`".$field."`";
				}
				$q_field = implode(' , ',$fields);
				//echo $w_field;
			}else{
				$q_field=" * ";
			}
			
			$wheres=[];
			if(array_key_exists('where',$act) && is_array($act['where']) && count($act['where'])>0 )
			{
				foreach($act['where'] AS $K=>$V){
					$wheres[]="`".$K."`='".$V."'";
				}
				$q_where = " WHERE ". implode(' AND ',$wheres);
				//echo $w_field;
			}else{
				$q_where=" WHERE 1=1 ";
			}
			
			
			$likes=[];
			if(array_key_exists('like',$act) && is_array($act['like']) && count($act['like'])>0 )
			{
				foreach($act['like'] AS $K=>$V){
					$likes[]="`".$K."` LIKE '%".$V."%'";
				}
				$q_like = " AND ". implode(' OR ',$likes);
				//echo $l_field;
			}else{
				$q_like="";
			}
			
			
			$orders=[];
			if(array_key_exists('order',$act) && is_array($act['order']) && count($act['order'])>0 )
			{
				foreach($act['order'] AS $K=>$V){
					$orders[]="`".$K."` ".$V;
				}
				$q_order = " ORDER BY ".implode(' , ',$orders);
				//echo $q_order;
			}else{
				$q_order="";
			}
			
			$Query = " SELECT ".$q_field." FROM `".$tbl."` ".$q_where.$q_like.$q_order;
			self::dbS();
			//echo $Query . "<br/>";
			$query_result = mysql_query($Query);
			if($query_result){
				$one_by_one=[];
				while( $row=mysql_fetch_object($query_result) ){
					$one_by_one[]=$row;
				}
				return $one_by_one;
			}else{
				return "ERROR!";
			}
			
			
		}else{
			die("Database Table Name Must Be Assigned!!!");
		}
	}
	// END OF SELECT QUERY
	
	
	// INSERT QUERY 
	
	
	static public function insert($act){
		if(is_array($act) && count($act)>0 ){
			if(array_key_exists('tbl',$act) && !empty($act['tbl']) ){
				$tbl=$act['tbl'];
				$query="INSERT INTO `".$tbl."`";
				if(array_key_exists('data',$act) && is_array($act['data']) && count($act['data'])>0  ){
						foreach($act['data'] AS $K=>$V){
							$keys[]="`".$K."`";
							$values[]="'".$V."'";
						};
						$key = implode(' , ',$keys);
						$val = implode(' , ',$values);
					$query="INSERT INTO `".$tbl."` (".$key.") values(".$val.")";
					Q::dbS();
					$r=mysql_query($query);
					//Debug::dd($r);
					if($r){
						return true;
					}else{
						return false;
					}
				}
			}else{
				die("Define Table Name!!!");
			}
		}
	}
	
	
	
	
}

?>